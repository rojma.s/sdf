//'use strict';
import I18n from 'react-native-i18n';

import * as languages from './langs'

Object.keys(languages).map((language)=>{
   I18n.translations[language]=languages[language].translation;
})
I18n.fallbacks = true;


export default I18n;