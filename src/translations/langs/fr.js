export default fr= {
    label: 'French',
    code: 'fr',
    translation: {
        "Send": "Envoyer le paiment",
        "Balance": "Solde",
        "Amount": "Montant",
        "Recipient": "bénéficiaire",
        "Hello": "Bonjour",
        "Choose Language": "Choisir la langue",
        "English": "Anglais",
        "French": "franÃ§ais",
        "UMVA Sign In": "Enregistrez-vous dans UMVA",
        "UMVA_ID": "Identification UMVA",
        "en": "en",
        "Payment": "Paiement",
        "QR Payment": "QR Paiement",
        "Account": "Compte",
        "Logout": "Logout",
        "Password": "Mot de passe",
        "Login": "Ouverture de session",
        "UMVA_ID_field_is_required": " Champ obligatoire",
        "user_does_not_exists": "utilisateur inexistant",
        "Password is not correct": "Mot de passe incorrecte",
        "User not exist":"utilisateur inexistant"
    }
}
