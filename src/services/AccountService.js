import {Api} from "./Api";

function getAccounts(params) {

    const res = Api.get('accountsByUser',
        {
            params
        }
    ).then((response) => {

        let acc = response.data.result;
        // let balance = response.data.result[0].currentbalance;
        return acc;
    });


    return res;

}

export const AccountService= {
 getAccounts
}