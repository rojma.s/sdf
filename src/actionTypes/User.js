export default UsernActionTypes={
   EMAIL_CHANGED : 'email_changed',
   PASSWORD_CHANGED : 'password_changed',
   USER_LOGIN : 'user_login',
   LENGTH_ERROR : 'length_error',
   USER_LOGIN_SUCCESS : 'user_login_success',
   USER_LOGIN_FAIL : 'user_login_fail',
   USER_DETAILS : 'user_details',
   LOGOUT : 'logout',
   LOGOUT_REQUEST : 'logout_request',
   USER_DETAIL_REQUEST:'user_detail_request',
    CLIENT_DETAIL_REQUEST:'client_detail_request',
    CLIENT_DETAIL:'client_detail',
    RESET:'reset'


}