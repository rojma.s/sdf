export default SdfActionTypes={
    SDF_LIST_FAIL:'sdf_list_fail',
    SDF_LIST_SUCCESS:'sdf_list_success',
    SDF_LIST_REQUEST:'sdf_list_request',
    SDF_FORM_DATA:'sdf_form_data',
    SDF_FORM_SUCCESS:'sdf_form_success',
    SDF_FORM_ERROR:'sdf_form_error',
}