export default AccountActionTypes={
      BALANCE_REQUEST :'balance_request' ,
  SEND_PAYMENTS_REQUEST :'send_payments_request' ,
  CONFIRM_REQUEST :'confirm_request' ,
  QR_REQUEST  : 'qr_request' ,
  CANCEL_PAYMENT_REQUEST :"cancel_payment_request" ,
    CLIENT_ACCOUNTS_SUCCESS :'client_accounts_success',
    CLIENT_ACCOUNTS_REQUEST : 'client_accounts_request',
    MERCHANT_ACCOUNTS_REQUEST: 'merchant_accounts_request',
    MERCHANT_ACCOUNTS_SUCCESS: 'merchant_accounts_success',
    MERCHANT_ACCOUNT_SELECTED: 'merchant_account_selected',
    MERCHANT_ACCOUNT_CHANGE_REQUEST:'merchant_account_change_request',
    CLIENT_ACCOUNT_CHANGE_REQUEST:'client_account_change_request',
    CLIENT_ACCOUNT_SELECTED: 'client_account_selected',
    RESET:'reset'

 }