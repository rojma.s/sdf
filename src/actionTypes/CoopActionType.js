export default CoopActionTypes={
    
    COOP_LIST_FAIL:'coop_list_fail',
    COOP_LIST_SUCCESS:'coop_list_success',
    COOP_LIST_REQUEST:'coop_list_request',
    SOURCE_LIST_REQUEST:'source_list_request',
    SOURCE_LIST_SUCCESS:'source_list_success',
    SOURCE_LIST_FAIL:'source_list_fail',
    COOP_VALUE_SELECTED:'coop_value_selected',
    SOURCE_VALUE_SELECTED:'source_value_selected',
    RESET:'reset',
    
}