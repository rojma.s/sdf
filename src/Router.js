import React, { Component } from 'react';
import { Scene, Router, Actions, Drawer } from 'react-native-router-flux';
import Login from './components/login/Login';

//  import QrComponent from './components/QrCode/QrComponent';
import Pay from './components/Payments/Pay';
import Receive from './components/Receive/Receive';
import ConfirmReceive from './components/Receive/ConfirmReceive'
import { connect } from 'react-redux';

import Confirm from '../src/components/Payments/Confirm';

import {DrawerComponent,DrawIcon} from './components/Drawer';
import Pillar from './components/Pillar/Pillar';

import Index from '../src/components/IndexComponent';
import { getUser } from './reducers/user';
import Registration from './components/Profile/Registration'

import {bindActionCreators} from "redux";
import * as languageUpdate from "./actions";

import SDF from './components/SDF/sdf';
class RouterComponent extends Component {

    constructor() {
        super();
        // let user = await this._getStorage();
    }

    componentWillMount() {


    }


    changedLanguage(value) {

        this.props.actions.languageUpdate({ prop: 'lang', value });
    }



    render() {
    const { isAuthenticated } = this.props;
    if(!isAuthenticated){
        return <Login/>;
    }
        return (
            <Router  >
                <Scene key="root" >
                    <Drawer key="main"
                        drawerIcon={<DrawIcon/>}

                        drawerWidth={240}
                        contentComponent={DrawerComponent} hideNavBar={true}
                        navigationBarStyle={{ backgroundColor: Colors.redhishOrange }}

                    >
                        <Scene
                            key="index"
                            component={Index}
                            title="UMVA-Pay"
                            titleStyle={{ color: "white", alignContent: 'center' }}
                        />

                        {/* <Scene
                            key='qr'
                            component={QrComponent}
                            title='QR Scanner'
                            titleStyle={{ color: "white", alignContent: 'center' }}

                        /> */}
                        <Scene
                            key='sdf'
                            component={SDF}
                            title='Sociodemographic Factors'
                            hideNavBar={false}
                            titleStyle={{ color: "white" }}
                        />
                        <Scene
                            key='problem'
                            component={Pillar}
                            title='Pillar'
                            hideNavBar={false}
                            titleStyle={{ color: "white" }}
                        />

                        <Scene key="pay" component={Pay} title='QR Payment'
                               titleStyle={{ color: "white", alignContent: 'center' }} />

                        <Scene
                            key='confirm'
                            component={Confirm}
                            title='Confirm Booking'
                            hideNavBar={false}
                            titleStyle={{ color: "white" }}
                        />
{/* 
                        <Scene
                            key='qr-Receive'
                            component={QrComponent}
                            title='QR Receive'
                            titleStyle={{ color: "white", alignContent: 'center' }}

                        /> */}

                        <Scene key="receive" component={Receive} title='QR Receive'
                               titleStyle={{ color: "white", alignContent: 'center' }} />


                        <Scene key="registrationInfo" component={Registration} title='Registration Info'
                               titleStyle={{ color: "white", alignContent: 'center' }} />

                        <Scene key="user" component={SDF} title='User Info'
                               titleStyle={{ color: "white", alignContent: 'center' }} />

                        <Scene
                            key='confirmReceive'
                            component={ConfirmReceive}
                            title='Confirm Booking'
                            hideNavBar={false}
                            titleStyle={{ color: "white" }}
                        />
                    </Drawer>


                </Scene>



            </Router>

        );
    }
};

const mapStateToProps = (state) => {

    return {
        isAuthenticated: state.auth.isAuthenticated
    }
};

function mapDispatchtoProps(dispatch) {
    return{
        actions: bindActionCreators(languageUpdate,dispatch),
    }

}

// export default RouterComponent;
export default connect(mapStateToProps, mapDispatchtoProps)(RouterComponent);


