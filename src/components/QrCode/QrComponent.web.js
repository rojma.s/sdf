'use strict';
import React, { Component } from 'react';
import {StyleSheet} from 'react-native';
import {Card} from '../common'
import { Cameras,Scanner} from "react-instascan";
import { qrRequest } from '../../actions';
import {Actions} from 'react-native-router-flux';
import {connect} from 'react-redux';

class QrComponent extends Component {

    constructor()
    {
        super();
        this.state={
            title: 'QR Payment'
        }
    }
    componentDidMount()
    {
        if(this.props.qrType=='qr')
            {
                this.setState({title: 'QR Payment'})
            }
            else {
                this.setState({title: 'QR Recieve'})
            }

    }
    _handleBarCodeRead(e) {
        // Vibration.vibrate();
        //  this.setState({scanning: false});
       
        let str=e.substring(e.search("UMVA ID:"));
       
        let data=str.substring(str.search(':')+1);
     
        if(str.search(':')!=7)
        {
            alert('invalid QR');
        }
        else
        {
            this.props.qrRequest(data.trim());
            if(this.props.qrType=='qr')
            {
                Actions.pay();
            }
            else {
                Actions.receive()
            }

        }

        // let txt=e.split(':');
        //  //  alert("here",e.data);
        //  if(txt[0] === 'UMVA ID')
        //  {
        //      this.props.qrRequest(txt[1]);
        //      Actions.pay();
        //  }
        //  else {
        //      alert('invalid QR')
        //  }
        //  Linking.openURL(e.data).catch(err => console.error('An error occured', err));


    }
    render() {
        return (
           <Card title={this.state.title}> 
            <Cameras style={{height:'100vh'}}>

                {cameras => (


                    <Scanner options={{scanPeriod:2,mirror:false}} camera={cameras[1]?cameras[1]:cameras[0]}  onScan={this._handleBarCodeRead.bind(this)} style={{height:'100vh'}}>
                        <video style={{ height:'100vh'}} />
                    </Scanner>

                )}
            </Cameras>
            </Card>
        );
    }

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        backgroundColor: 'black'
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    capture: {
        flex: 0,
        backgroundColor: '#fff',
        borderRadius: 5,
        padding: 15,
        paddingHorizontal: 20,
        alignSelf: 'center',
        margin: 20
    }
});
const mapStateToProps = (state) =>{

    return{
        qrType:state.pay.qrType
    }
};

export default connect(mapStateToProps, {qrRequest })(QrComponent);