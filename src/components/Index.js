import React, { Component } from 'react';
import { Card, CardSection, Input } from './common';
import { Image, Text, View,ImageBackground, TouchableOpacity, ScrollView, BackHandler } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { confirmPayment, userAccount, languageUpdate,userDetail } from '../actions';
import { connect } from 'react-redux';
//import Toast from 'react-native-simple-toast';
import I18n from '../translations/i18n';
class IndexComponent extends Component {

    constructor() {
        super();
        this.state = {
            english: "black",
            french: "#ff6f00",
        }
    }


    changedLanguage(value) {
        //console.log("The value is", value);
        this.props.languageUpdate({ prop: 'lang', value });



    }
    enPressed() {
        // //console.log("H");
        this.setState({
            english: 'black',
            french: '#ff6f00'
        })
        this.changedLanguage("en");
    }

    frPressed() {
        // //console.log("H");
        this.setState({
            french: 'black',
            english: '#ff6f00'
        });
        this.changedLanguage("fr");

    }


    componentWillMount() {
        if (this.props.messages) {
          //  Toast.show(this.props.messages, Toast.CENTER);
            BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);


        }
    }

    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
        //console.log(this.props);
        this.props.userDetails(this.props.id);

    }
    payPressed() {
        //console.log("Pressed")
        this.props.userAccount(this.props.id, "pay");

    }


    componentWillUnmount() {
       // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    showAlert() {
        // ToastAndroid.show('Under Construction', ToastAndroid.SHORT);
        this.props.userAccount(this.props.id, "qr");
    }
    render() {
        return (
            // <ScrollView>
            // <Card style={{justifyContent:'flex-start', alignItems:'flex-start'}}>
            //     <CardSection style={{marginBottom:20}}>
            //         <Image style={styles.image} source={require('../images/user.png')}/>
            //         <View style={styles.containerStyle}>
            //             <Text style={styles.labelStyle}>Test User</Text>
            //             <Text style={styles.subText}>Demo Version</Text>
            //         </View>

            //     </CardSection>

            //     <CardSection style={{marginBottom:10}}>
            //     <Image style={styles.QRimage} source={require('../images/pay.png')}/>
            //         <TouchableOpacity style={
            //             styles.containerStyle
            //             }
            //             onPress={this.payPressed.bind(this)}
            //             >
            //         <Text style={{color:'#ff6f00', fontSize:22, marginTop:10}}> 
            //         {I18n.t("Payment")}</Text>
            //         </TouchableOpacity>   
            //     </CardSection>

            //     <CardSection style={{marginBottom:10}}>
            //         <Image style={styles.QRimage} source={require('../images/qr.png')}/>
            //         <TouchableOpacity 
            //             style={styles.containerStyle}
            //             onPress={this.showAlert.bind(this)}
            //             >
            //             <Text style={{
            //                 color:'#ff6f00',
            //                 fontSize:22,
            //                 marginTop:10
            //                 }}>
            //                 QR Payment
            //                 </Text>
            //             </TouchableOpacity>

            //     </CardSection>


            // </Card>
            // </ScrollView>

            <ImageBackground
            style = {{
                    flex: 1,
                    alignSelf:'stretch'
                }}
        source = { require('../images/ombre.jpg') }
            >
            <View style={{  flex: 1, flexDirection: 'column' }}>
                <View style={{ backgroundColor: 'white', margin: 5, }}>
                    <Text style={{ fontSize: 18, color: '#ff6f00' ,fontWeight:'bolds'}}>{I18n.t("Welcome to UMVA Payment")}</Text>
                </View>

                <View style={{ flexDirection: 'row', flex: .35 }}>
                    <View style={{ flex: 1, marginTop: 5, marginLeft: 5, borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderTopColor:'#e8e8e8',borderRightColor:'#e8e8e8',borderLeftColor: '#e8e8e8', borderBottomColor:'#fde2b1', }}>
                        <TouchableOpacity onPress={this.showAlert.bind(this)}>
                            <Image source={require('../images/qrlogo.png')} style={{ alignSelf: 'center', justifyContent: 'center' }} />
                            <Text style={{ color: '#ffad14', fontWeight:'bold',fontSize: 18, marginTop: 20 }}>{I18n.t("QR PAYMENT")}</Text>
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 1, marginTop: 5, marginLeft: 5, borderWidth: 1, borderTopColor:'#e8e8e8',borderRightColor:'#e8e8e8',borderLeftColor: '#e8e8e8', borderBottomColor:'#fde2b1',alignItems: 'center', justifyContent: 'center', marginRight: 5 }}>
                        <TouchableOpacity onPress={this.payPressed.bind(this)}>
                            <Image source={require('../images/payment.png')} style={{ alignSelf: 'center', justifyContent: 'center' }} />
                            <Text style={{ color: '#ffad14',fontWeight:'bold', fontSize: 18, marginTop: 20 }}>{I18n.t("PAYMENT")}</Text>
                        </TouchableOpacity>
                    </View>

                </View>

                <View style={{ flexDirection: 'row', flex: .35 }}>
                    <View style={{ flex: 1, marginTop: 5, marginLeft: 5, borderWidth: 1, alignItems: 'center', justifyContent: 'center',borderColor:'#fde2b1'}}>
                        <Image source={require('../images/trans.png')} style={{ alignSelf: 'center', justifyContent: 'center' }} />
                        <Text style={{ color: '#fff4e0', fontWeight:'bold', fontSize: 18, marginTop: 20 }}>{I18n.t("TRANSACTIONS")}</Text>

                    </View>

                    <View style={{ flex: 1, marginTop: 5, marginLeft: 5, borderWidth: 1, borderColor: '#fde2b1', alignItems: 'center', justifyContent: 'center', marginRight: 5 }}>
                        <Image source={require('../images/user.png')} style={{ alignSelf: 'center', justifyContent: 'center' }} />
                        <Text style={{ color: '#fff4e0', fontWeight:'bold',fontSize: 18, marginTop: 20 }}>{I18n.t("USER DETAILS")}</Text>

                    </View>

                </View>

                <View style={{ flexDirection: 'row', flex: .3 }}>
                    <View style={{ flex: 1, marginTop: 5, marginLeft: 5, borderWidth: 1, alignItems: 'center', justifyContent: 'center', borderColor: '#fde2b1', marginRight: 5, marginBottom: 5 }}>

                    </View>

                </View>
            </View>
            </ImageBackground>
        );
    }
}

const styles = {
    image: {
        height: 100,
        borderRadius: 100,
        width: 100,
        borderColor: '#ff6f00',
        borderWidth: 1

    },
    textBox: {
        marginTop: 13,
        fontSize: 20,
        color: '#ff6f00'
    },
    subText: {
        fontSize: 15,
        color: '#ff6f00'
    },
    QRimage: {
        height: 100,
        width: 100,
        borderRadius: 100,
        borderColor: '#ff6f00',
        borderWidth: 1


    },
    QRtext: {
        color: '#ff6f00',

        fontSize: 20,
        marginTop: 20
    },
    labelStyle: {
        fontSize: 20,
        color: '#ff6f00'
    },
    containerStyle: {
        flex: 1,
        flexDirection: 'column',
        marginLeft: 10,
        alignItems: 'flex-start'

    }
}

const mapStateToProps = (state) => {
    // //console.log("The index state is", state);
    return {
        messages: state.pay.messages,
        id: state.auth.user.user_id,
        lang: state.auth.lang
    }
}

export default connect(mapStateToProps, { confirmPayment, userAccount, languageUpdate,userDetails })(IndexComponent);