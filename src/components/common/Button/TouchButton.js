import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import {
    StyleSheet,
    TouchableOpacity,
    Text,
    View,

    Dimensions
} from 'react-native';

import I18n from '../../../translations/i18n';


const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;
const MARGIN = 40;

export default class TouchButton extends Component {
    constructor() {
        super();




    }



    render() {
        return (


            <TouchableOpacity
                style={styles.button}
                {...this.props}>

                <View style={styles.container}>
                    <Icon name={this.props.icon}  style={styles.inlineImg} color={this.props.iconcolor} />
                    <Text style={styles.text}> {I18n.t(this.props.text)}</Text>
                </View>

            </TouchableOpacity>



        );
    }
}
TouchButton.propTypes = {
    icon: PropTypes.string,
    iconcolor:PropTypes.string,
    text: PropTypes.string,
};

const styles = StyleSheet.create({
    container: {
        //  flex: 1,
        alignItems: 'center',
        // justifyContent: 'flex-start',
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#f05a0c',
        height: MARGIN,
        borderRadius: 20,
        width:'100%',
        zIndex: 100,
    },
    circle: {
        height: MARGIN,
        width: MARGIN,
        marginTop: -MARGIN,
        borderWidth: 1,
        borderColor: '#F035E0',
        borderRadius: 100,
        alignSelf: 'center',
        zIndex: 99,
        backgroundColor: '#F035E0',
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    },
    image: {
        width: 24,
        height: 24,
    },
});
