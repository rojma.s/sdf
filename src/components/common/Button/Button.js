import React from 'react';
import { Text, TouchableOpacity } from 'react-native';

const Button = ({ onPress, children, style }) => {

    const { buttonStyle, textStyle } = styles;
    return (
                <TouchableOpacity
                style={[buttonStyle]}
                    onPress={onPress}
                >
                    <Text style={[textStyle,style]}>{children}</Text>
                </TouchableOpacity>

    );
};


const styles = {

    textStyle: {
        alignSelf: 'center',
        color: 'white',
        fontSize: 16,
        fontWeight: '600',
        paddingTop: 10,
        paddingBottom: 10

    },

    buttonStyle: {
        flex: 1,
        alignSelf: 'stretch',
        backgroundColor: '#ff6f00',
        borderRadius: 5,
        marginLeft: 5,
        marginRight: 5,

    }
};
export default Button ;
