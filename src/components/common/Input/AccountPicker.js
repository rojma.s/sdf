import React, {Component} from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import {StyleSheet, View, Picker,Dimensions,Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import styles from './styles';4
import {

    userAccounts,
    accountSelected

} from "../../../actions";
import I18n from '../../../translations/i18n';
class AccountPicker extends Component {
    constructor(props)
    {
        super(props);


    }

    componentDidMount()
    {

        this.props.userAccounts(this.props.userId,this.props.accountFor);


    }


   changeAccount(value)
    {
        var result=this.props.Accounts.find(obj=>obj.account_id==value);

        this.props.accountSelected(result,this.props.accountFor);
    }


    render() {


        return (
            <View style={{marginTop:2, marginBottom:20,flex:1, flexGrow:50}}>
                {this.props.errortext ? <Text style={{color:'#ff0000',fontSize:16,marginBottom:10,paddingLeft:30}}>
                <Icon name={'warning'} color={'#ff0000'}></Icon>{I18n.t(this.props.errortext)} </Text>:null }
                {this.props.accountFor=='Merchant'?
                
                <Text style={{fontWeight:'bold'}}>{I18n.t('Current Balance')+' : '+this.props.selectedAccount.currentbalance}</Text>
                
                :null}
                <View style={styles.container}>
                                 
                    {this.props.label?<Text style={styles.label}>{this.props.label}</Text>:null}
                    <View style={styles.iconWrapper}>
                    <Icon name={this.props.icon} style={styles.icon} color={this.props.iconcolor} />
                    </View>
                    <View style={styles.inputWrapper}>

                    <Picker
                        selectedValue={this.props.selectedAccount.account_id}
                        onValueChange={
                        value=> this.changeAccount(value)}
                    style={styles.input}
                    {...this.props}>
                    {this.props.Accounts.map((item,index)=> {
                            return <Picker.Item

                                style={styles.items} key={index} label={item.bank_name + '(' + item.bankaccount + ')'} value={item.account_id}/>
                        }
                    )}

                </Picker>
                    </View>

            </View>
            </View>
        );
    }
}

AccountPicker.propTypes = {
    icon: PropTypes.string,
    iconcolor: PropTypes.string,
    userId: PropTypes.number,
    errortext: PropTypes.string,
    accountFor: PropTypes.string,
    label:PropTypes.string

};
const mapStateToProps = (state,props) => {

    return {
        merchantAccounts: state.accounts.merchantAccounts,
        selectedMerchant:state.accounts.selectedMerchant,
        clientAccounts:state.accounts.clientAccounts,
        selectedClient:state.accounts.selectedClient,
        Accounts: props.accountFor=='Merchant'?state.accounts.merchantAccounts:state.accounts.clientAccounts,
        selectedAccount: props.accountFor=='Merchant'?state.accounts.selectedMerchant:state.accounts.selectedClient
    }
}
export default connect(mapStateToProps, {
    userAccounts,
    accountSelected
})(AccountPicker);
