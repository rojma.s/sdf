import {StyleSheet} from "react-native";

export default styles = StyleSheet.create({
    input: {
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
        width:'100%',
        color: '#4b4b4b',
        borderWidth:0,
        height:'100%'

    },

    container: {
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent:'center',
        width:'100%'

    },
    inputWrapper: {
        height:40,
        flex:10,
        borderWidth: 1,
        borderColor:'rgba(0, 0, 0, 0.1)',
        borderTopRightRadius:10,
        borderBottomRightRadius:10,
        borderLeftWidth:0,
        borderLeftColor:'transparent',
        backgroundColor: 'rgba(255, 255, 255, 0.4)',
    },
    label:
    {
        fontSize:16,
        width:100,
    },
    iconWrapper: {
        height:40,
        flex:1,
        borderWidth: 1,
        borderColor:'rgba(0, 0, 0, 0.1)',
        borderTopLeftRadius:10,
        borderBottomLeftRadius:10,
        borderRightWidth:0,
        borderRightColor:'transparent',
        paddingTop:8,
        paddingBottom:8,
        paddingLeft:10,
        backgroundColor: 'rgba(255, 255, 255, 0.4)',

        //flex: 1,


    },
    icon:{
        fontSize:21,
    }
});