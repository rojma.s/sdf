import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {StyleSheet, View, TextInput, Image,Dimensions,Text} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from '../../../translations/i18n'
import styles from './styles'

export default class UTextInput extends Component {
    constructor(props)
    {
        super();

    }
    render() {
        return (
            <View style={{marginBottom:20,height:50}}>
                {this.props.errortext ? <Text style={{color:'#ff0000',fontSize:16,marginBottom:10,paddingLeft:30}}><Icon name={'warning'} color={'#ff0000'}></Icon>{I18n.t(this.props.errortext)} </Text>:null }
            <View style={styles.container}>
            {this.props.label?<Text style={styles.label}>{this.props.label}</Text>:null}
                <View style={styles.iconWrapper}><Icon name={this.props.icon}  style={styles.icon} color={this.props.iconcolor} /></View>
                <View style={styles.inputWrapper}>
                <TextInput
                    style={[styles.input,this.props.numeric=='true'?{textAlign:'right'}:'']}

                    {...this.props}

                    underlineColorAndroid="transparent"
                />
                </View>
            </View>
            </View>
        );
    }
}

UTextInput.propTypes = {
    icon: PropTypes.string,
    iconcolor:PropTypes.string,
    placeholder: PropTypes.string,
    secureTextEntry: PropTypes.bool,
    autoCorrect: PropTypes.bool,
    autoCapitalize: PropTypes.string,
    returnKeyType: PropTypes.string,
    errortext:PropTypes.string,
    label:PropTypes.string,
    numeric: PropTypes.string
};
