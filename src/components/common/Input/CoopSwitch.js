import React, { Component } from 'react'
import { View, Switch, StyleSheet  } from 'react-native'


export default class CoopSwitch extends Component {
  state = {
   
    colorFalseSwitchIsOn: false,
  };

  render() {
    return (
      <View>
        <Switch
          onValueChange={(value) => this.setState({colorFalseSwitchIsOn: value})}
          onTintColor="#00ff00"
          style={{marginBottom: 10}}
          thumbTintColor="#0000ff"
          tintColor="#ff0000"
          value={this.state.colorFalseSwitchIsOn} />
        
      </View>
    );
  }
}     