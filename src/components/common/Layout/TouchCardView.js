import React,{Component} from 'react';
import PropTypes from 'prop-types';
import {View, TouchableOpacity, Text,StyleSheet} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import {TouchButton} from "../Layout";
import I18n from '../../../translations/i18n';

class TouchCardView extends Component{
    constructor(props)
    {
        super();
    }

    render()
    {
        if(this.props.text){
            return(
                <TouchableOpacity onPress={this.props.onPress} >
                    <View style={[this.styles.containerStyle]}>
                        <Icon name={this.props.icon} color={this.props.iconColor} size={this.props.iconSize}/>
                        <Text style={this.props.textStyle}>{I18n.t(this.props.text)}</Text>
                    </View>
                </TouchableOpacity>
            );
        }
        else {
            return(
                <TouchableOpacity onPress={this.props.onPress} >
                    <View style={[this.styles.containerStyle]}>
                        <Icon name={this.props.icon} color={this.props.iconColor} size={this.props.iconSize}/>

                    </View>
                </TouchableOpacity>
            );
        }

    }

    get styles(){
        return StyleSheet.create(
            {
                containerStyle: {
                    borderWidth: 1,
                    borderRadius: 10,
                    borderColor: '#ddd',
                    borderBottomWidth: 0,
                    shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.1,
                    shadowRadius: 2,
                    elevation: 1,
                    height:'100%',
                    width:'100%',

                    justifyContent:"center",
                    alignItems: 'center',
                    padding:5,
                    backgroundColor:'#ffffff'

                }
            }
        );
    }
}
TouchCardView.propTypes = {
    icon: PropTypes.string,
    iconColor:PropTypes.string,
    iconSize:PropTypes.number,
    text: PropTypes.string,
    textStyle:PropTypes.object,
    onPress: PropTypes.func
};
export default TouchCardView;
