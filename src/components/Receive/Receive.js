import React, { Component } from "react";
import { Image, StyleSheet, Keyboard,KeyboardAvoidingView, Dimensions, ScrollView, Text, TouchableOpacity, View, TextInput, Picker } from "react-native";
import Icon from "react-native-vector-icons/MaterialIcons";
import { CardSection, Card, Input, Button } from "../common";
import TouchButton from '../common/Button/TouchButton';
import {
    userDetail,
    userAccounts,
    paymentUpdate,
    lengthError,
    sendPayments,
    confirmPayment,
    accountBalance,
    accountSelected
} from "../../actions";
import { connect } from 'react-redux';
//import Toast from 'react-native-simple-toast';
// import FlashMessage,{ showMessage } from "react-native-flash-message";
//import KeyboardSpacer from 'react-native-keyboard-spacer';
import I18n from '../../translations/i18n';
import UTextInput from "../common/Input/UTextInput";
import AccountPicker from '../common/Input/AccountPicker';
import Spinner from "../common/Utility/Spinner";

class Receive extends Component {




    getOrientation() {

        if (this.state.screen.width > this.state.screen.height) {
            return 'LANDSCAPE';
        } else {
            return 'PORTRAIT';
        }
    }

    getStyle() {
        if (this.getOrientation() === 'LANDSCAPE') {
            return landscapeStyles;
        }
        else {
            return portraitStyles;
        }
    }
    onLayout() {
        this.setState({ screen: Dimensions.get('window') });
    }

    constructor() {
        super();
        this.state = {
            Acc: [],
            selected: '',
            loading:true,
            screen: Dimensions.get('window'),

        }
    }


    componentWillMount() {


    }




    componentWillUnmount() {
       // BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    renderButton() {

        return (
            <Button onPress={this.onButtonPress.bind(this)}>
                {I18n.t("Send")}
            </Button>
        );
    }

    onButtonPress() {
        const { recipient, description, amount } = this.props;

        Keyboard.dismiss();

        this.props.sendPayments({ accounts: this.props.selectedMerchant, recipient, description, amount,receive:true });

    }

    renderError() {
        if (this.props.error) {
            return (
                // showMessage({
                //     message: this.props.error,
                //     type: "danger",
                //   })
                <View style={{backgroundColor: 'white',height: 40,
                    flexDirection: 'row',}}>
                    <Text style={styles.errorStyle}>
                        {this.props.error}
                    </Text>
                </View>

            );
        }
    }

    showBalance() {


        if (this.props.balanceA) {
            return (
                <View style={{height: 40,
                    flexDirection: 'row',
                    alignItems: 'center' }}

                >
                    <Text style={{fontSize: 18,
                        paddingLeft: 20,
                        flex: 1,color:'#ff6f00'}}
                    >
                        {I18n.t("Balance")} : {this.props.balanceA}
                    </Text>
                </View>

            );
        }
    }

    componentDidMount() {
        this.props.userAccounts(this.props.merchantId,'Merchant');

        var result = this.props.acc.find(obj => obj.account_id == this.props.id.default_account_id);


        this.props.accountSelected(result, 'Merchant');

    }



    renderBalance(value){

        this.props.accountBalance(value);
    }



    changedValue(value){
        this.props.paymentUpdate({ prop: 'account', value });
        this.renderBalance(value);

    }
    changeSender(value)
    {
        this.props.paymentUpdate({ prop: 'recipient', value });
        // this.props.userDetail(value)

    }




    render() {



        return (
            <KeyboardAvoidingView behavior="padding">
             <Card title={I18n.t('Receive')}>
                <View style={this.getStyle().container} onLayout={this.onLayout.bind(this)}>
                    { this.props.loading ? <Spinner/>: null }

                  

                    <View style={this.getStyle().innerView}>
                        {this.props.error? this.props.error.general?<View><Text style={{color:'red'}} >{this.props.error.general}</Text></View>:null:null}
                            <UTextInput placeholder={I18n.t("Sender")}
                            label={I18n.t("Sender")}
                                        value={this.props.recipient}
                                        autoCapitalize='none'
                                        onChangeText={value =>this.changeSender(value) }
                                        icon={"id-badge"} iconcolor={"#ff6600"}
                                        errortext={this.props.error?this.props.error.recipient:null}

                            />
                         
                      
                            {/*<AccountPicker iconcolor={"#ff6600"} icon={"bank"} accountFor={"Client"} userId={this.props.id.detail?this.props.id.detail.userid:null}/>*/}
                     
                        
                            <UTextInput placeholder={I18n.t("Amount")}
                            label={I18n.t("Amount")}
                                        value={this.props.amount}
                                        autoCapitalize='none'
                                        onChangeText={value => this.props.paymentUpdate({ prop: 'amount', value })}
                                        icon={"money"} iconcolor={"#ff6600"}
                                        keyboardType = 'numeric'
                                        errortext={this.props.error?this.props.error.amount:null}
                                        numeric={'true'}
                                       />

                           
                      
                            <UTextInput placeholder={I18n.t("Description")}
                            label={I18n.t("Description")}
                                        value={this.props.description?this.props.description:'Payment From UMVA'}
                                        autoCorrect={false}
                                        autoCapitalize='none'
                                        onChangeText={value => this.props.paymentUpdate({ prop: 'description', value })}
                                        icon={"info"} iconcolor={"#ff6600"}
                                      
                                        errortext={this.props.error?this.props.error.description:null}

                            />

                        


                        <TouchButton  onPress={this.onButtonPress.bind(this)} text={"Send"}/>

                    </View>


                </View>
                </Card>
            </KeyboardAvoidingView>

        );
    }
}

const portraitStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin: 5,
    },
    input: {
        flex: 0.2,
    },
    innerView: {
        flex: 0.7,
        flexDirection: 'column',
    },
    inputText: {

        flex: 0.2, flexDirection: 'row', alignItems: 'center',

    },
    lang: {
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        flex: .20
    },
    button: {

        padding: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ffffe0',
        backgroundColor: '#ff6f00',
        // elevation: 1,
        alignSelf: 'center',
        width: '40%',
        marginTop: 20, height: 60

    },
    btn:{
        color: 'white',
        fontSize: 16,
        width: '100%',
        textAlign: 'center',
        fontWeight: '400',
        paddingLeft: 10
    }
});

const landscapeStyles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        margin:"auto",
        maxWidth:600,
        minWidth:300
    },
    input: {

        flex: .5,
    },
    innerView: {
        flex: 1,
        flexDirection: 'column',

    },
    inputText: {
        flex: 0.5, flexDirection: 'row', alignItems: 'center', height: 40,

    },

    lang: {
        paddingBottom: 20,
        flexDirection: 'row',
        justifyContent: 'flex-end',
        flex: .40
    },
    button: {

        padding: 5,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#ffffe0',
        backgroundColor: '#ff6f00',
        // elevation: 1,
        alignSelf: 'center',
        width: '25%', marginTop:20,height: 60

    },
    btn:{
        color: 'white',
        fontSize: 16,
        width: '100%',
        textAlign: 'center',
        fontWeight: '400',
        paddingLeft: 10
    }
});


const styles = StyleSheet.create({
    container: {
        backgroundColor: '#CCCCCC',
        padding: 15,
        display: 'flex',
        width: '100%',
        paddingTop: 50
    },
    inputStyle: {
        color: '#000',
        paddingRight: 2,
        paddingLeft: 2,
        fontSize: 18,
        lineHeight: 23,
        flex: 0.5

    },
    labelStyle: {
        fontSize: 18,
        paddingLeft: 15,
        color: '#ff6f00',
        paddingTop: 15
    },container: {
        backgroundColor: "#ff6f00",
        borderRadius: 2,
        marginLeft: 7,
        marginRight: 7,
        paddingTop: 10,
        paddingBottom: 10,
        flexDirection: "row",
        paddingLeft: 5,
        paddingRight: 5

    },
    errorStyle: {
        color: 'red',
        fontSize: 18,
        paddingLeft: 20,
        flex: 1,
        alignContent:'center'
    },
    infoStyle: {
        color: '#838383',
        fontSize: 15,
        flexDirection: 'row',
        alignItems: 'center'
    },
    heading: {
        fontSize: 18,
        fontWeight: '500',
        color: '#ff6f00',
        padding: 8
    },
    inputBox: {
        padding: 5,
        width: '50%',
        borderRadius: 10,
        paddingHorizontal: 25,
        fontSize: 16,
        backgroundColor: 'white',
        justifyContent: 'flex-end',

    },
    inputText: {
        padding: 5,
        fontSize: 16,
        fontWeight: '500',
        color: 'white',
        flex: 1

    },
    button: {
        marginLeft: '60%',
        backgroundColor: "white",
        paddingLeft: 30,
        paddingRight: 30,

        borderRadius: 25,

    },
    buttonText: {
        padding: 5,
        fontSize: 16,
        fontWeight: '500',
        color: '#ff6f00',

    },
    pickerStyle: {
        fontSize: 18,
        paddingLeft: 20, color: '#ff6f00', backgroundColor: 'red'
    }

});



const mapStateToProps = (state) => {


    const { amount, nwerror,recipient, description, account ,  messages,loading, code, error,balance} = state.pay;

    return {
        id: state.auth.user,
        acc: state.user.merchantAccounts,
        user_id:state.auth.user_id,
        balanceA: state.pay.balance,
        autherror:state.auth.autherror,
        payAccount:state.auth.payAccount,
        merchantId:state.auth.user.user_id,
        selectedMerchant:state.accounts.selectedMerchant,
        amount,
        recipient,
        description:state.pay.description?state.pay.description: state.user.detail.umva_id +' payment through UMVA',
        account,
        loading,
        error,
        messages,
        balance,
        nwerror
    };
};

export default connect(mapStateToProps, {
    userDetail,
    userAccounts,
    paymentUpdate,
    sendPayments,
    confirmPayment,
    lengthError,
    accountBalance,
    accountSelected
})(Receive);
