import React, { Component } from 'react';
import {Card} from '../common'
import { connect } from 'react-redux';
import {Text,Image,View,ScrollView} from 'react-native'
import I18n from '../../translations/i18n'
 class Registration extends Component {
    render(){
        const{umva_id,first_name,last_name,umva_cardid,birthday,gender,address_country,mobile,phone}=this.props.detail;
        return(

            <Card title={I18n.t('User Detail')}>
                <View style={{ backgroundColor: Colors.redhishOrange, justifyContent: 'center', alignItems: 'center' }}>
                <Image source={{uri: this.props.detail?this.props.detail.umva_photo_jpg_pending?"data:image/png;base64, "+this.props.detail.umva_photo_jpg_pending: require('../../images/logo.png'):require('../../images/logo.png')}}
                       style={{
                           width: 160,
                           height: 160,
                           borderRadius: 160,
                           marginTop: 5,
                           marginBottom: 10
                       }}/>
                </View>
                <ScrollView>
                <Card>
                <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('UMVA ID')} : </Text>
                    <Text style={{color: '#939393'}}> {umva_id} </Text>
                    </View>
                </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Given Name')} : </Text>
                    <Text style={{color: '#939393'}}> {first_name} </Text>
                    </View>
                        
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Last Name')} : </Text>
                    <Text style={{color: '#939393'}}> {last_name} </Text>
                    </View>
                            
                    
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('ID Card')} : </Text>
                    <Text style={{color: '#939393'}}> {umva_cardid} </Text>
                    </View>
                    
                    
                    
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Birthday')} : </Text>
                    <Text style={{color: '#939393'}}> {birthday} </Text>
                    </View>
                    
                    
                    
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Gender')} : </Text>
                    <Text style={{color: '#939393'}}> {gender} </Text>
                    </View>
                    
                   
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Country')} : </Text>
                    <Text style={{color: '#939393'}}> {address_country} </Text>
                    </View>
                    
                    
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Mobile')} : </Text>
                    <Text style={{color: '#939393'}}> {mobile} </Text>
                    </View>
                        
                    </Card>
                    <Card>
                    <View style={{flex:1,flexDirection:'row'}}>
                    <Text style={{fontWeight:"bold"}}>{I18n.t('Phone')} : </Text>
                    <Text style={{color: '#939393'}}> {phone} </Text>
                    </View>
                        
                    </Card>

                </ScrollView>
            </Card>
        );
    }

}

const mapStateToProps = (state) => {


    return {
        detail:state.user.detail,

    }
};
export default connect(mapStateToProps,{})(Registration);