import React, { Component } from "react";
import { connect } from 'react-redux';
import { Text,View,StyleSheet,ScrollView }  from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import I18n from '../../translations/i18n'
import {
    paymentList,
    userAccounts
} from '../../actions'


class PaymentList extends Component {
    constructor() {
        super();
    }

    renderData() {

        {
            Object.keys(this.props.list).map((item) => {
                return item;
            });
        }
    }

    componentDidMount() {//action lae call garney tarika

        this.props.paymentList(this.props.default.account_id, this.props.detail.umva_id, this.props.default.value_type_id);


    }

    render() {


        return (
            <View style={{flex:1}}>
                <View style={{height:30,flexDirection:'row',justifyContent:'center',backgroundColor:'#c6c6c6'}} >
                    <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold'}}>{I18n.t('Date')}</Text></View>
                    <View style={{flex:3,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold'}}>{I18n.t('From')+'/'+I18n.t('To')}</Text></View>
                    <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold',textAlign:'right'}}>{I18n.t('Amount')}</Text></View>
                    <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold'}}>{I18n.t('Value')}</Text></View>
                    <View style={{flex:3,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold'}}>{I18n.t('Description')}</Text></View>
                    <View style={{flex:1,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,fontWeight:'bold'}}>{I18n.t('In')+'/'+I18n.t('Out')}</Text></View>
                </View>
                <ScrollView style={{flex:1}}>
                {Object.values(this.props.list).reverse().map((item, key) => {
            return (<View style={{flex:1,flexDirection:'row',justifyContent:'center'}} key={key}>
                <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10}}>{item.stamp.substring(0,8)}</Text></View>
                <View style={{flex:3,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10}}>{item.debit>0 ? item.loginto : item.loginto}</Text></View>
                <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10,textAlign:'right'}}>{item.debit>0?item.debit:item.credit}</Text></View>
                <View style={{flex:2,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10}}>{item.value}</Text></View>
                <View style={{flex:3,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1}}><Text style={{fontSize:10}}>{item.details}</Text></View>
                <View style={{flex:1,borderWidth:1,borderColor:'rgba(0,0,0,0.4)',padding:1,justifyContent:'center',alignItems:'center'}}>{item.debit>0?<Icon name={"arrow-left"} style={styles.icon} color={'blue'} ></Icon>:<Icon name={"arrow-right"} style={styles.icon} color={'red'} ></Icon>}</View>
            </View>)
        })}
                </ScrollView>
            </View>
        )

    }

}

const styles= StyleSheet.create(
    {
        row:{
            flex:1,
            flexDirection: 'row',
            borderColor:'rgba(0, 0, 0, 0.1)',
            borderWidth:1

        },
        date:
            {
               width:'16%',
                borderColor:'#c6c6c6',
                borderWidth: 1,
                padding: 5,
                fontSize:12,
                overflow:'hidden'

            },
          to:
              {
                  width:'64%',
                  borderColor:'#c6c6c6',
                  borderWidth: 1,
                  padding: 5,
                  overflow:'hidden'

              },
           amount:{
            width:'20%',
               borderColor:'#c6c6c6',
               borderWidth: 1,
               padding: 5,
        overflow:'hidden'

           },
           value:
               {
                width:'15%',
                   borderColor:'#c6c6c6',
                   borderWidth: 1,
                   padding: 5,
        overflow:'hidden'

               },
            detail:
                {
                 width:'20%',
                    borderColor:'#c6c6c6',
                    borderWidth: 1,
                    padding: 5,
                    overflow:'hidden'

                },
        icon:{
            fontSize:14,

        }
    })



const mapStateToProps = (state) => {

 return{
     list: state.pay.paymentList,
     detail:state.user.detail,
     default: state.accounts.selectedMerchant,
     id:state.auth.user.user_id,
 }
}
export default connect(mapStateToProps, {
    paymentList,
    userAccounts
})(PaymentList);

