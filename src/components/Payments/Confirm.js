import React, { Component } from 'react';
import { 
    Image, 
    StyleSheet, 
    Text, 
    TouchableOpacity, 
    View, 
    TextInput, 
   // ToastAndroid,
    BackHandler,
    Picker,
    ScrollView,
    KeyboardAvoidingView
} from "react-native";
import { Card, CardSection,Button,Input} from '../common';
import { connect } from 'react-redux';
import {sendPayments,paymentUpdate,confirmPayment, cancelPayment} from '../../actions';

import UTextInput from "../common/Input/UTextInput";
import I18n from "../../translations/i18n";
import AccountPicker from "../common/Input/AccountPicker";
import TouchButton from "../common/Button/TouchButton";
import Spinner from '../common/Utility/Spinner'
class Confirm extends Component{


    constructor() {
        super();
        this.state = {
          Ac: '',
          selected: ''
        }
      }
      componentDidUpdate(){
        if(this.props.nwerror){
           // Toast.show(this.props.nwerror, Toast.CENTER);
      
          }
    }
    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    }
    

    componentWillUnmount() {
      //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }



      renderButton() {
        
        return (
          <Button onPress={this.onButtonPress.bind(this)}>
            Confirm 
          </Button>
          
        );
      }

      renderCancelButton() {
        if (this.props.eloading) {
            return <Spinner size='large' />;
          }
          return (
            <Button onPress={this.onCancel.bind(this)}>
              Cancel
            </Button>
            
          );
      }
    
      onButtonPress() {

        const {  recipient, description, amount,faccount } = this.props;

        let rec ='';
        if(this.props.raccount){
             rec=this.props.raccount
        }
        else{
            rec=this.state.selected
        }

        
        this.props.confirmPayment({ account: rec, recipient, description, amount ,faccount});
      }

      onCancel(){
          this.props.cancelPayment();
      }

      renderError() {
        if (this.props.error) {
            return (
                // showMessage({
                //     message: this.props.error,
                //     type: "danger",
                //   })
                <View style={{ backgroundColor: 'white' }}>
                    <Text style={styles.errorStyle}> 
                        {this.props.error}
                    </Text>
                </View>
                
            );
        }
    }

    renderMessage(){
        showMessage({
            message: "Simple message",
            type: "danger",
          });
    }
          

    render(){


        return(
            <KeyboardAvoidingView behavior="padding">
<Card title={I18n.t('Confirm Payment')}>
           
            <View style={styles.container}>
            {this.props.loading? <Spinner/> :null}
               <View style={styles.innerView}>
                    <UTextInput placeholder={I18n.t("Amount")}
                     label={I18n.t("Amount")}
                                value={this.props.amount}
                                autoCorrect={false}
                                autoCapitalize='none'

                                icon={"money"} iconcolor={"#ff6600"}
                               editable={false}
                                numeric={'true'}

                    />
                
                    <UTextInput placeholder={I18n.t("To")}
                     label={I18n.t("To")}
                                value={this.props.recipient}
                                autoCorrect={false}
                                autoCapitalize='none'

                                icon={"user"} iconcolor={"#ff6600"}
                                editable={false}

                    />
             
                    <UTextInput placeholder={I18n.t("Description")}
                    label={I18n.t("Description")}
                                value={this.props.description}
                                autoCorrect={false}
                                autoCapitalize='none'

                                icon={"info"} iconcolor={"#ff6600"}
                                editable={false}

                    />
              
                    <UTextInput placeholder={I18n.t("Cost")}
label={I18n.t("Cost")}
                                autoCorrect={false}
                                autoCapitalize='none'
                                value={'0'}
                                icon={"money"} iconcolor={"#ff6600"}
                                editable={false}
                                numeric={'true'}

                    />
            
                    <AccountPicker iconcolor={"#ff6600"} icon={"bank"} label={I18n.t("Account To")} accountFor={"Client"} userId={this.props.clientId}/>
       

                    <TouchButton  onPress={this.onButtonPress.bind(this)} text={"Confirm"} />

                

          </View>
            </View>
            </Card>
            </KeyboardAvoidingView>
            
        );
    }
}
const mapStateToProps = (state) =>{

    return{

        raccount:state.accounts.selectedClient.account_id,
        faccount:state.accounts.selectedMerchant.account_id,
        loading:state.pay.loading,
        error:state.pay.error,
        eloading:state.pay.eloading,
        nwerror:state.pay.nwerror,
        amount:state.pay.amount,
        description:state.pay.description,
        recipient:state.pay.recipient,
        clientId:state.pay.client.userid
    }
};

const styles = {
    pickerStyle: {
        fontSize: 18,
        paddingTop: 10,
        paddingLeft: 15,
        flex: 1,
      },
      errorStyle: {
        color: 'red',
        fontSize: 15,
        alignSelf: 'center'
    },
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent:'space-around',
        margin:"auto",
      
       
    },
    innerView:
    {
      flex:1,
      flexDirection: 'column',
      justifyContent:'center'
    }
}


export default  connect(mapStateToProps,{sendPayments,paymentUpdate,
    confirmPayment,
    cancelPayment
})(Confirm);