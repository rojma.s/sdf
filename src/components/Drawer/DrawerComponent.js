import React, {Component} from "react";
import {
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
    Image,
    ScrollView,
} from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";
import {Actions} from "react-native-router-flux";
import {connect} from 'react-redux';

import * as drawerAction from '../../actions/index'
import I18n from '../../translations/i18n';
import LanguagePicker from '../common/Input/LanguagePicker';
import Colors from '../common/Utility/Colors';
import {bindActionCreators} from 'redux';

import SDF from '../SDF/sdf';

class DrawerComponent extends Component {

    constructor() {
        super();
        this.state = {
            user: '',
            image: ''
        }
    }

    componentDidMount() {
        this.props.actions.userDetail(this.props.id);
    }

    logout() {
        this.props.actions.logout();
        Actions.pop();
    }

    payPressed() {
        Actions.pay();
    }

    receivePressed() {
        Actions.receive();

    }
    sdfPressed() {
        // alert('you have pressed');
        Actions.sdf();
        
    }

governancePressed(){
    Actions.problem();
}
    


    qrpay(qrType) {
        this.props.actions.userAccount(this.props.id, qrType, 'Merchant');
    }

    render() {

        return (
            <ScrollView style={styles.container}>
                <View style={{backgroundColor: Colors.redhishOrange, justifyContent: 'center', alignItems: 'center'}}>
                    <Image
                        source={{uri: this.props.detail ? this.props.detail.umva_photo_jpg_pending ? "data:image/png;base64, " + this.props.detail.umva_photo_jpg_pending : require('../../images/logo.png') : require('../../images/logo.png')}}
                        style={{
                            width: 80,
                            height: 80,
                            borderRadius: 80,
                            marginTop: 5,
                            marginBottom: 10
                        }}/>

                    <Text
                        style={{
                            fontSize: 15,
                            color: 'white',
                            marginLeft: 10,
                            marginBottom: 10
                        }}

                    >{this.props.umva_id}</Text>

                </View>
                <LanguagePicker selectedValue={this.props.lang} icon={"language"} iconcolor={Colors.redhishOrange}/>
                <TouchableOpacity
                    style={styles.menuItem}
                    onPress={Actions.index}
                >
                    <Icon name="home" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>

                    <Text style={styles.menuItemText}>{I18n.t('Home')}</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.menuItem}
                    onPress={this.payPressed.bind(this)}
                >
                    <Icon name="money" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>

                    <Text style={styles.menuItemText}>{I18n.t("Payment")}</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.menuItem}
                    onPress={this.sdfPressed.bind(this)}
                >
                    <Icon name="money" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>

                    <Text style={styles.menuItemText}>SDF</Text>
                </TouchableOpacity>
                <TouchableOpacity
                    style={styles.menuItem}
                    
                >
                    <Icon name="money" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>
                    
                    {/* <Icon name="icon_greater"  color={Colors.redhishOrange} style={{marginLeft: 5}}/> */}

                    <Text style={styles.menuItemText}>Problems</Text>

                </TouchableOpacity>
                
                <View style={styles.navSectionStyle}>
          <Text onPress={this.governancePressed.bind(this)} style={styles.navItemStyle}>
            Governance
          </Text>
          
        </View>
                

                <TouchableOpacity
                    style={styles.menuItem}
                    onPress={this.qrpay.bind(this)}
                >
                    <Icon name="qrcode" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>

                    <Text style={styles.menuItemText}>QR Payment</Text>
                </TouchableOpacity>

                <TouchableOpacity
                    style={styles.menuItem}
                    onPress={() => this.qrpay('qr')}
                >
                    <Icon name="list" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>

                    <Text style={styles.menuItemText}>Transactions</Text>
                </TouchableOpacity>

                <TouchableOpacity style={styles.menuItem} onPress={() => this.logout()}>
                    <Icon name="power-off" size={21} color={Colors.redhishOrange} style={{marginRight: 5}}/>
                    <Text style={styles.menuItemText}>{I18n.t("Logout")}</Text>
                </TouchableOpacity>

            </ScrollView>
        );
    }
}

const styles = StyleSheet.create({
    container: {},
    menuItem: {
        padding: 7,
        // backgroundColor: "ivory",
        marginBottom: 2,
        margin: 5,
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignItems: 'center'
    },
    menuItemText: {
        fontSize: 18,
        color: Colors.redhishOrange,
        paddingTop: 2
    },
    navItemStyle: {
        fontSize: 16,
        color: Colors.redhishOrange,
        padding: 2
      },
      navSectionStyle: {
        marginLeft: 40
      }
});


const mapStateToProps = (state) => {


    return {
        balance: state.auth.balance,
        id: state.auth.user.user_id,
        umva_id: state.auth.umva_id,
        lang: state.language.language,
        detail: state.user.detail,

    }
};

function mapDispatchtoProps(dispatch) {
    return {
        actions: bindActionCreators(drawerAction, dispatch),
    }

}

export default connect(mapStateToProps, mapDispatchtoProps)(DrawerComponent);
