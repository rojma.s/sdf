import React, {Component} from 'react';
import {connect} from 'react-redux';
import Spinner from '../common/Utility/Spinner'
import {
    View,
    Text,
    NetInfo,
    TextInput
} from 'react-native';
import I18n from '../../translations/i18n';

import {LanguagePicker, UTextInput, TouchButton} from '../common'
import {stylesForm} from './styles'
import {bindActionCreators} from "redux";
import * as loginAction from "../../actions";

class Form extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoading: false
        }

    }

    handleBackButton() {
        return true;
    }

    onEmailChange(text) {

        this.props.actions.emailChanged(text);

    }

    onPasswordChange(text) {
        this.props.actions.passwordChanged(text);
    }

    onButtonPress() {
        const {umva_id, password} = this.props;

        NetInfo.isConnected.fetch().then(isConnected => {
            if (isConnected) {

                this.setState({isLoading: true})
                this.props.actions.userlogin({umva_id, password});

                this.setState({isLoading: false})

            }
            else {
                console.log("Error");
            }
        })
    }

    render() {

        return (


            <View style={stylesForm.container}>
                {this.props.loading ? <Spinner/> : <Text></Text>}
                <LanguagePicker icon={"language"} iconcolor={"#ff6600"}/>
                <UTextInput
                    icon="user"
                    iconcolor="#ff6600"
                    placeholder={I18n.t("UMVA ID")}
                    autoCapitalize={'none'}
                    returnKeyType={'done'}
                    autoCorrect={false}
                    onChangeText={this.onEmailChange.bind(this)}
                    errortext={this.props.error ? this.props.error.umva_id : null}

                />
                <UTextInput
                    icon="lock"
                    iconcolor="#ff6600"
                    secureTextEntry={true}
                    placeholder={I18n.t("Password")}
                    autoCapitalize={'none'}
                    returnKeyType={'done'}
                    autoCorrect={false}
                    onChangeText={this.onPasswordChange.bind(this)}
                    onSubmitEditing={this.onButtonPress.bind(this)}
                    errortext={this.props.error ? this.props.error.password : null}

                />
                
               
                <TouchButton onPress={this.onButtonPress.bind(this)} text={"Login"} icon={"lock"}/>
            </View>


        );
    }
}

const mapStateToProps = (state) => {
   // console.log('error',state.auth.error);
    return {
        umva_id: state.auth.umva_id,
        password: state.auth.password,
        error: state.auth.error,
        loading: state.auth.loading,
        nwerror: state.auth.nwerror,
        lang: state.language.language
    };
};

function mapDispatchtoProps(dispatch) {
    return {
        actions: bindActionCreators(loginAction, dispatch),
    }

}

export default connect(mapStateToProps, mapDispatchtoProps)(Form);


