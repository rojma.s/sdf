import React from 'react';
import Form from './Form';
import {
    View,
    Image,
    KeyboardAvoidingView,

} from 'react-native-web';
import umvaPayemnt from '../../images/logo.png';
import Colors from '../common/Utility/Colors'

import {ResponsiveComponent} from "react-native-responsive-ui";
import {sytleLayout} from "./styles";
import {networkFail} from '../../actions/index';

class Login extends ResponsiveComponent {

    constructor(props) {
        super(props);

    }


    render() {

        return (
            <KeyboardAvoidingView behavior="padding" style={this.styles.container}>

                <View style={this.styles.logoContainer}>
                    <Image source={umvaPayemnt} style={this.styles.img}/>
                </View>
                <View style={this.styles.formContainer}>
                    <Form/>
                </View>

            </KeyboardAvoidingView>
        );

    }

    get styles() {
        return sytleLayout
    }
}

export default Login;


