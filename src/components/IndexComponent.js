import React from 'react';
import {TouchCardView,Card} from './common'
import {  View, ScrollView, BackHandler} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from 'react-redux';
import {ResponsiveComponent,ResponsiveStyleSheet} from 'react-native-responsive-ui';
import PaymentList from './Payments/PaymentList';
import I18n from '../translations/i18n';
import {bindActionCreators} from "redux";
import * as indexAction from "../actions";

class Index extends ResponsiveComponent {

    constructor(props) {
        super(props);

    }


    componentDidMount() {
        BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);

    }
    payPressed() {

       // this.props.merchantAccounts(this.props.id, "pay");
        Actions.pay();

    }
    receivePressed() {

        // this.props.merchantAccounts(this.props.id, "pay");
        Actions.receive();

    }
    componentDidUpdate() {
        if (this.props.nwerror) {
            // Toast.show(this.props.nwerror, Toast.CENTER);

        }
    }

    componentWillUnmount() {
      //  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
    }

    handleBackButton() {
        // ToastAndroid.show('Back button is pressed', ToastAndroid.SHORT);
        return true;
    }

    showAlert(qrType) {
        // ToastAndroid.show('Under Construction', ToastAndroid.SHORT);
        this.props.actions.qrPaymentRequest(this.props.id, qrType,"Merchant");
    }


    render() {

        return (
            <View style={this.styles.container}>
                <View style={this.styles.menuContainer}>
                    {(this.props.gender=='company' || this.props.gender=='group')?
                    <View style={this.styles.menuHalf}>

                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"qrcode"}  iconColor={"#ff6600"} iconSize={50} text={I18n.t("QR Receive")} textStyle={{fontSize:24,color:'#ff6600'}}  onPress={()=>this.showAlert('qr-receive')} />

                        </View>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"money"}  iconColor={"#ff6600"} iconSize={50} text={I18n.t("Receive")} textStyle={{fontSize:24,color:'#ff6600'}}  onPress={this.receivePressed.bind(this)} />

                        </View>

                    </View> :null}
                    <View style={this.styles.menuHalf}>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"qrcode"}  iconColor={"#ff6600"} iconSize={50} text="QR Payment" textStyle={{fontSize:24,color:'#ff6600'}}  onPress={()=>this.showAlert('qr')} />

                        </View>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"money"}  iconColor={"#ff6600"} iconSize={50} text="Payment" textStyle={{fontSize:24,color:'#ff6600'}}  onPress={this.payPressed.bind(this)} />

                        </View>

                    </View>
                    <View style={this.styles.menuHalf}>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"user"}  iconColor={"#ff6600"} iconSize={50} text="User Detail" textStyle={{fontSize:24,color:'#ff6600'}}  onPress={()=>{Actions.registrationInfo()}} />

                        </View>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"user"}  iconColor={"#ff6600"} iconSize={50} text="User " textStyle={{fontSize:24,color:'#ff6600'}}  onPress={()=>{Actions.user()}} />

                        </View>
                        <View style={this.styles.menuBox}>

                            <TouchCardView icon={"list"}  iconColor={"#ff6600"} iconSize={50} text="Transaction" textStyle={{fontSize:24,color:'#ff6600'}}  onPress={()=>{return false}} />

                        </View>

                    </View>
                </View>
                <View style={this.styles.transaction}>
                        <Card title={I18n.t("Transaction")}>

                            {this.props.account.account_id? <ScrollView><PaymentList/></ScrollView> : null}


                        </Card>

                </View>
                
            </View>

        );

    }

    get styles(){
        return ResponsiveStyleSheet.select(
            [
                {
                    query:{orientation:'landscape'},
                    style:{
                        container:{
                            flex:1,
                            flexDirection:'row',

                        },
                        menuHalf:{
                            flex:0.5,
                            flexDirection:'row',
                            width:'100%',

                            justifyContent:'center',
                            alignItems:'center'
                        },
                        menuContainer:
                            {
                                flex:1,
                                width:'30%',
                                flexDirection:'column',

                                justifyContent:'center',
                                alignItems:'center'
                            },
                        menuBox:
                            {
                                flex:0.5,
                                width:'40%',
                                margin:10,
                                // backgroundColor:'#ffffff'

                            },
                        transaction:
                            {
                                flex:1,
                                width:'70%',
                                //backgroundColor: Colors.darkestOrange
                            }

                    }
                },
                {
                    query:{orientation:'portrait'},
                    style:{
                        container:{
                            flex:1,
                            flexDirection:'column',
                            justifyContent:'space-around'
                        },
                        menuHalf:{
                            flex:0.5,
                            flexDirection:'row',
                            width:'100%',
                          //  backgroundColor: Colors.orange,
                            justifyContent:'space-between',
                            alignItems:'center'
                        },
                        menuContainer:
                            {
                                flex:0.5,
                                width:'100%',
                                flexDirection:'column',
                               // backgroundColor: Colors.redhishOrange,
                                justifyContent:'space-between',
                                alignItems:'center'
                            },
                        menuBox:
                            {
                                flex:0.5,
                                width:'40%',
                                margin:10,
                               // backgroundColor:'#ffffff'

                            },
                        transaction:
                            {
                                flex:0.5,
                                width:'100%',

                            }


                    }
                }

            ])
    }
}




const mapStateToProps = (state) => {

    return {
        messages: state.pay.messages,
        id: state.auth.user.user_id,
        lang: state.language.language,
        gender: state.user.detail.gender,
        account: state.accounts.selectedMerchant,
    }
}
function mapDispatchtoProps(dispatch) {
    return{
        actions: bindActionCreators(indexAction,dispatch),
    }

}

export default connect(mapStateToProps,mapDispatchtoProps)(Index);
