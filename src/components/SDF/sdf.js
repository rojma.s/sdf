import React, { Component } from 'react';
import {
    View,
   StyleSheet,  
Text,
ScrollView,
TouchableOpacity,

} from 'react-native-web';
import CoopProductPicker from '../common/Input/CoopProductPicker';
import CoopSwitch from '../common/Input/CoopSwitch';
//import Spinner from '../common/Utility/Spinner';

import {FlatList,RefreshControl, ActivityIndicator, Button, TextInput} from 'react-native';
//import {stylesForm} from '.././login/styles'
//import {TouchButton} from '../common'
import {connect}  from 'react-redux';
import {sdfList,storeSdfReport} from '../../actions'


class SDF  extends Component{
    constructor(props) {
        super(props);
        this.state = {
            isLoading: false,
            name:'',
            formData:[{name:''}],
            
                    
        }
        this.renderFlatlistItem=this.renderFlatlistItem.bind(this);
        this.onButtonPress=this.onButtonPress.bind(this);
        this.textHandle=this.textHandle.bind(this);
        
    }

    componentDidMount() {//action lae call garney tarika

        this.props.sdfList();
    }

    renderFlatlistItem(item,index)
    {
        let colors = [ '#fdecba', '#abcdef'];
        return(
            <View style={{flex:1, flexDirection:'row', backgroundColor: colors[index % colors.length]}}> 
                <View style={styles.factor}>
                    <Text>{item.list}</Text>
                    
                </View>
               
                <View style={styles.value}>
                {(item.id=== 9 || item.id===10) ?
                    <CoopProductPicker/>
                    : (item.id===32 || item.id===37||item.id===38 || item.id===33 )?<CoopSwitch/> : <TextInput ref={item.id} value={this.state.formData.name} name={'name'+item.id} onChange={this.textHandle(index,event)}/>

                
            }
 
                </View>
                
                <View style={styles.update}>
                    <Text>Update</Text>
                </View>

            </View>
            
        )

    }
    renderHeader() {
  return (
      
    <View style={{flex:1, 
    flexDirection:'row', 
    backgroundColor:'orange',
    borderRadius:'25px',
    border: '2px solid #FF6900',
     fontWeight:'bold',
     fontSize:10,
     padding:'15px',
     //width: '200px',
  height: '50px',

     }} >
    <View style={{flex:3}}>
         <Text >SDF</Text>
    </View>
    <View style={{flex:2}}>
        <Text >Value</Text>
    </View>
    <View style={{flex:1}}>
        <Text >Update</Text>
    </View>
     
    </View>
  )
}
renderSeparator() {
    return <View style={styles.separator} />
  }
  
  renderFooter(){
      return <View>
          <Text> Auxfin nepal 2018</Text>
      </View>
  }
  
  
  onButtonPress=() => {
    // const {umva_id, password} = this.props;

    // NetInfo.isConnected.fetch().then(isConnected => {
    //     if (isConnected) {

    //         this.setState({isLoading: true})
    //         this.props.actions.userlogin({umva_id, password});

    //         this.setState({isLoading: false})

    //     }
    //     else {
    //         console.log("Error");
    //     }
    // })
    //this.setState(formData)
    var payload =this.state.formData;
    console.log('test', payload);
    this.props.storeSdfReport();
    console.log("eventsss",this.state.formData);

    //alert('u have summited the form');
}

textHandle=(index) => (event)=>{
//    // event.preventDefault();
//     const array=[];

    
//    // this.refs.name27;
    const  value= event.target.value;
//    const  name = event.target.name;
    console.log(value);
//    console.log(name);
//    array.push([{'name':name,'value':value}]);
//   console.log(array);
//   this.setState({name:value});
//   this.setState({formData:this.state.formData.concat(array)});
//   this.setState(prevState => ({
//     formData: [...prevState.formData, {"name": "value"}]
  //}))
   //this.state.formData.push(array);
   const newShareholders = this.state.formData.map((form, sidx) => {
    if (index == sidx) return form;
    return { ...form, name: event.target.value };
  });
console.log('evnets', newShareholders);
  this.setState({ formData: newShareholders });
    

}

  
  
    render(){
        return(
            <ScrollView >
                <View style={styles.container}>
                {!this.props.loaded?
                <View style={styles.spinnerStyle}>
                    <ActivityIndicator size='large' />
                </View>
                :null}
                
                <FlatList style={styles.factor}
                 ListHeaderComponent ={ this.renderHeader}
                
                ItemSeparatorComponent={this.renderSeparator}
                data={this.props.list}
                keyExtractor={(item,index)=>item.list + index}
               renderItem={({item,index}) => this.renderFlatlistItem(item,index)}
               
               
                
                />
              
              {/* <Button onPress={()=>this.onButtonPress()} component={TouchableHighlight} underlayColor="red" title={"Save"} 
       style={styles.button}/> */}
       <TouchableOpacity onPress = {() =>this.onButtonPress() }>
                <View style = {styles.button}
                       >
                    <Text style = {{color: 'white'}}>Save</Text>
                </View>
            </TouchableOpacity>

                </View>
                
                
                
            </ScrollView>
        );
    }
}

const mapStateToProps = (state) =>{
    // console.log('sdf',state.sdf.sdfList);
    return{
        list:state.sdf.list,
        loaded:state.sdf.loading,
    }
}
export default connect(mapStateToProps, {
    sdfList, storeSdfReport
})(SDF);

const styles=StyleSheet.create({
 container:{
     flex:1,
    // flexDirection:'row',
     
        //alignItems: 'stretch',
        //backgroundColor:"red",
        alignItems:'stretch',
        
        //padding: '2em',
        margin:'20px',
        borderRadius: '25px'
        //shadowColor:'black',


 },
  factor:{
      flex:3,
      
     //backgroundColor:'white',
     
     //justifyContent: 'center'
  },
  value:{
      flex:2,
      justifyContent:'center',
     //backgroundColor:'green',
  },
  update:{
      flex:1,
    // backgroundColor:'blue',
},
separator:{
    height: 1,
   // width: "990%",
    //alignSelf: 'center',
    backgroundColor: "#555",
    //marginLeft: "14%"
    
},

spinnerStyle: {
    position:'absolute',
    zIndex:9999,
    width:'100%',
    height:'100%',
    backgroundColor:'rgba(255,255,255,0.5)',
    alignItems: 'center',
    justifyContent: 'center'
},

footerStyle:{
    
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  
},
button:{
    borderRadius:'15px',
    
    border: '2px solid #4CAF50',
    flex: 2,
    backgroundColor: 'red',
    alignItems: 'center',
    justifyContent: 'center',
    

}

// text: {
//     fontSize: 30,
//     alignSelf: 'center',
//     color: 'red'
//  }
})


