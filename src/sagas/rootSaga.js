import { call , all ,fork,takeLatest} from 'redux-saga/effects';
import {watchUserAccountsMerchant,
    watchQrPyament,
    watchPaymentList,
    watchUserAccountsClient,
    watchClientAccountChange,
    watchMerchantAccountChange,
    watchLoginRequest,
    watchUserDetail,
    watchBalanceRequest,
    watchSendPayments,
    watchConfirmRequest,
     watchCancelRequest, 
     watchLogoutRequest,
    watchSdfList,
    watchFormUpload,
watchCoopList} from './index';
import {changeLanguage} from './languageSaga'
import init from './init';

export default function* rootSaga() {

    yield fork(init);
    yield fork(changeLanguage);
     yield fork(watchLoginRequest);
     yield fork(watchUserDetail);
      yield fork(watchUserAccountsMerchant);
      yield fork(watchMerchantAccountChange);
      yield fork(watchSendPayments);
      yield fork(watchUserAccountsClient);
      yield fork(watchClientAccountChange);
      yield fork(watchQrPyament);
      yield fork(watchPaymentList);
      yield fork(watchSdfList);
      yield fork(watchCoopList);
    yield fork(watchFormUpload);

    yield fork(watchConfirmRequest);


    
}
