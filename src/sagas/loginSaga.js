import {UserActionTypes} from "../actionTypes";

import { put, takeLatest, take, call } from 'redux-saga/effects';

import {AuthService} from "../services/AuthService";
import { Actions as act } from 'react-native-router-flux';

export function*  watchLoginRequest() {

    while (true) {
        const { umva_id, password } = yield take(UserActionTypes.USER_LOGIN);
        const payload = {
            "umva_id":umva_id,
            "password":password
        }

   const user=yield call(AuthService.userLoginFromApi,payload)
        if(user.status)
        {
            yield put({ type: UserActionTypes.USER_LOGIN_FAIL, error:user, loading: false });
        }
        else {
            yield put({ type: UserActionTypes.USER_LOGIN_SUCCESS, user: user,umva_id:payload.umva_id, loading: false });
          //  act.index();
            act.index();
        }

    }
}





