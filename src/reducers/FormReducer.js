import {SdfActionTypes} from '../actionTypes';
const INITIAL_STATE = { //initial state
    
    loading:false, 
    // nwerror:false,
     error:{},  
};

export default ( state=INITIAL_STATE, action ) =>{
    switch(action.type){
        case SdfActionTypes.SDF_FORM_DATA:
        return {...state, loading:false};

        // case SdfActionTypes.SDF_FORM_SUCCESS:
        // return {...state, data:action.list,loading:true};

        // case SdfActionTypes.SDF_FORM_ERROR:
        // return {...state, error:action.error};

        default:
            return state;
    }
}