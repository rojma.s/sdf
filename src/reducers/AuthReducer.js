import {AccountActionTypes, UserActionTypes} from '../actionTypes'

const INITIAL_STATE = {

    umva_id: '',
    password: '',
    user: '',
    error: {},
    loading: false,
    user_acc:'',
    autherror:'',
    payAccount:'',
    isAuthenticated:false
};

export default function auth( state=INITIAL_STATE, action ) {

    switch(action.type){
        case UserActionTypes.EMAIL_CHANGED:
            return { ...state, loading:false,umva_id: action.payload, error:'', autherror:'' };
        case UserActionTypes.PASSWORD_CHANGED:
            return { ...state,loading:false, password: action.payload , error:'', autherror:''};
        case UserActionTypes.LENGTH_ERROR:
            return{...state,autherror:action.payload.message, payAccount:action.payload.acc}
        case UserActionTypes.USER_LOGIN:
            return { ...state, error: '',loading:true };
        case UserActionTypes.USER_LOGIN_SUCCESS:

            return {
                user: action.user,
                loading:false,
                umva_id:action.umva_id,
                password:'',
                isAuthenticated:true,
                error:''
             };


        case UserActionTypes.USER_LOGIN_FAIL:
            return { ...state, error: action.error ,loading: false,  };

        default:
            return state;
    }
}