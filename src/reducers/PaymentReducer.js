
import {PaymentActionTypes} from '../actionTypes'

const INITIAL_STATE = { //initial state
    eloading:false,
    loading:false, 
    description:'',
    amount:'',
    recipient:'',
    nwerror:false,
    error:{},
    recipient_id:'',
    paymentList:[]
};

export default ( state=INITIAL_STATE, action ) => {

    switch(action.type){
        case PaymentActionTypes.USER_ACCOUNTS:
             return {...state, error:'',user_acc: action.acc, balance: action.balance,description:'',amount:'',recipient:''};
        case PaymentActionTypes.PAYMENT_UPDATE:
            return {...state,[action.payload.prop]:action.payload.value, error:''};
        case PaymentActionTypes.SEND_PAYMENTS_REQUEST:
            return {...state, loading: true,error:''};
        case PaymentActionTypes.SEND_PAYMENT_FAIL:
            return {
                ...state,

                error:action.error ,
                loading: false,  
            };
        case PaymentActionTypes.PAYMENT_CHECK_SUCCESS:
            return{...state,
                amount:action.amount,
                recipient:action.recipient,
                description:action.description,
                confirm:action.recipient_acc,
                user_acc:action.user_acc,
                client:action.recipient_id,
                loading:false
            }
        case PaymentActionTypes.RECIPIENT_ACCOUNTS:
            return { confirm:action.payload,
                loading:false, amount:'', description:'',recipient:'' ,
                error:''
            };
        case PaymentActionTypes.CONFIRM_REQUEST:
            return {...state,loading:true};
        case PaymentActionTypes.PAYMENT_CONFIRM:
            return{ loading:true};
        case PaymentActionTypes.CONFIRM_FAIL:
            return{ loading:false, error:action.payload, };
        case PaymentActionTypes.PAYMENT_SUCCESS:
            return INITIAL_STATE;
        case PaymentActionTypes.CANCEL_PAYMENT:
            return{...state,messages:'You have cancel the transaction'};

        case PaymentActionTypes.QR_PAYMENT_REQUEST:
        return {
            ...state,id:action.id,qrType:action.value
        }
        case PaymentActionTypes.QR_SUCCESS:

            return{
                ...state,recipient:action.recipient,qrType:action.qrType
            }
        case PaymentActionTypes.PAYMENT_LIST_REQUEST:
            return{
                ...state,loading:false
            }
        case PaymentActionTypes.PAYMENT_LIST_SUCCESS:
            return{
                ...state,paymentList: action.list
            }
        case PaymentActionTypes.PAYMENT_LIST_FAIL:
            return{
                ...state,error: action.error
            }
        case PaymentActionTypes.RESET:
            return INITIAL_STATE;
        default:
            return state;
    }
}