import {SdfActionTypes} from '../actionTypes';

const INITIAL_STATE = { //initial state
    //eloading:false,
    loading:false, 
    //description:'',
    //amount:'',
    //recipient:'',
    nwerror:false,
    error:{},
    //recipient_id:'',
    list:[]//yo list same
};
export default ( state=INITIAL_STATE, action ) => {
    switch(action.type){
        case SdfActionTypes.SDF_LIST_REQUEST:
            return{...state, loading:false}

        case SdfActionTypes.SDF_LIST_SUCCESS:
        return{...state, list:action.list, loading:true}

        case SdfActionTypes.SDF_LIST_FAIL:
        return{...state, error: action.error}
        
        default:
        return state

    }
}