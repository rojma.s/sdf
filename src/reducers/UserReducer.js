import {UserActionTypes} from "../actionTypes";

const INITIAL_STATE={
    detail:{},
    merchantAccounts:[],
    clientAccounts:[],
    clientDetail:{},
    error:{}
}

export default function userDetail(state=INITIAL_STATE,action)
{
    switch (action.type)
    {
        case UserActionTypes.USER_DETAIL_REQUEST:
            return {...state,loading:true}
        case UserActionTypes.USER_DETAILS:
          if(action.recipient)
          {
              return{...state,clientDetail:action.detail}
          }
          else{
            return{
                ...state,
                detail: action.detail
            }
          }
            
        case UserActionTypes.MERCHANT_ACCOUNTS_REQUEST:
            return {...state,loading:true}
        case UserActionTypes.MERCHANT_ACCOUNTS_SUCCESS:

            return{
                ...state,
                merchantAccounts: action.merchantAccounts
            }
        case UserActionTypes.RESET:
            return INITIAL_STATE
        default:
            return state;
    }
}