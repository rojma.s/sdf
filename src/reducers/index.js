import { combineReducers } from 'redux';
import AuthReducer from './AuthReducer';
import PaymentReducer from './PaymentReducer';
import LanguageReducer from './LanguageReducer'
import UserReducer from "./UserReducer";
import AccountReducer from "./AccountReducer"
import {UserActionTypes} from "../actionTypes";
import SdfReducer from './SdfReducer';
import CoopReducer from './CoopReducer';
import FormReducer from './FormReducer';
const appReducer =  combineReducers({
    auth: AuthReducer,
    language: LanguageReducer,
    pay: PaymentReducer,
    user: UserReducer,
    accounts:AccountReducer,
    sdf:SdfReducer,
    coop:CoopReducer,
    upload:FormReducer,
});

function index(state, action) {
    if (action.type === UserActionTypes.LOGOUT_REQUEST) {
        state = undefined;
    }

    return appReducer(state, action);
};
export default index;