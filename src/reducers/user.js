import {UserActionTypes} from '../actionTypes';

const initialState = {
  isAuthenticated: false,
  isFetching: false,
  token: '',
  user: {},
  errorMessage: '',
};

export default function user(state = initialState, action) {
  switch(action.type) {
    // case types.LOGIN.REQUEST:
    //   return Object.assign({}, state, {
    //     isFetching: true,
    //     isAuthenticated: false,
    //   });
      case  UserActionTypes.USER_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: true,
        token: action.token,
        failure: false,
        user: action.user,
      });
    case UserActionTypes.USER_LOGIN_FAIL:
      return Object.assign({}, state, {
        isFetching: false,
        isAuthenticated: false,
        failure: true,
        errorMessage: action.err,
      });
    case UserActionTypes.LOGOUT:
      return initialState;
    default:
      return state;
  }
}
