import {LanguageActionTypes} from '../actionTypes'
import I18n from '../translations/i18n';

const initialState={language:'en'}

export default function languageUpdate(state=initialState,action)
{

    switch(action.type)
    {
        case LanguageActionTypes.LANGUAGE_UPDATE:
            I18n.locale=action.language;

            return Object.assign({},state,{language:action.language});
        default:
          return state
    }
}