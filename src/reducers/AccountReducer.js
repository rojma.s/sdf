import {AccountActionTypes} from '../actionTypes'
const INITIAL_STATE={
    selectedMerchant:{},
    merchantAccounts:[],
    clientAccounts:[],
    selectedClient:{},
    error:{},

}

export default function AccountData(state=INITIAL_STATE,action)
{
    switch (action.type)
    {
        case AccountActionTypes.MERCHANT_ACCOUNTS_REQUEST:
            return {...state,loading:true}
        case AccountActionTypes.MERCHANT_ACCOUNTS_SUCCESS:
            return{
                ...state,
                merchantAccounts: action.merchantAccounts
            }
        case AccountActionTypes.MERCHANT_ACCOUNT_CHANGE_REQUEST:
        return{
            ...state,
            loading:true
        }

        case AccountActionTypes.MERCHANT_ACCOUNT_SELECTED:
            return{
                ...state,
                selectedMerchant:action.selectedMerchant
            }
        case AccountActionTypes.CLIENT_ACCOUNTS_REQUEST:
            return{
                ...state,
                loading:true
            }
        case AccountActionTypes.CLIENT_ACCOUNTS_SUCCESS:

            return{
                ...state,
                clientAccounts: action.clientAccounts
            }
        case AccountActionTypes.CLIENT_ACCOUNT_SELECTED:
            return{
                ...state,
              selectedClient:action.selectedClient
            }
        case AccountActionTypes.RESET:
            return INITIAL_STATE;
        default:
            return state;

    }
}