import React, { Component } from 'react';
import { View, KeyboardAvoidingView } from 'react-native';
import { Provider } from 'react-redux';


import Router from './Router';
import configureStore from '../src/store/configureStore';


class App extends Component {
    constructor() {
        super();
        this.state = {
          isLoading: true,
          //store: store(() => this.setState({isLoading: false})),
          //store: store
        };
      }
    render() {
        console.disableYellowBox = true;

        return (
            <Provider store={configureStore()}>
                <Router />

            </Provider>

        );
    }
}

 //saga.run(rootSaga);

export default App;