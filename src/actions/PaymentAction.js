//import { AsyncStorage } from 'react-native';
import {PaymentActionTypes} from "../actionTypes";

export const paymentUpdate=({prop, value })=>{

    return{
        type:PaymentActionTypes.PAYMENT_UPDATE,
        payload: {prop, value}

    }
};

export const confirmPayment = ({account, recipient, description, amount,faccount,recieve,password})=>{
    return {
        type:PaymentActionTypes.CONFIRM_REQUEST,
        account,
        recipient,
        description,
        amount,
        faccount,
        recieve,
        password

    }
};

export const cancelPayment = () => {
    return{
            type: PaymentActionTypes.CANCEL_PAYMENT_REQUEST ,
        
            
        };
        
    
};

export function paymentList(account_id,umva_id,value_type_id)
{
    return{
        type:PaymentActionTypes.PAYMENT_LIST_REQUEST,
        account_id:account_id,
        umva_id:umva_id,
        value_type_id:value_type_id
    }
}

export function sendPayments(payload) {
    return {
        type:PaymentActionTypes.SEND_PAYMENTS_REQUEST,
        accounts:payload.accounts,
        recipient:payload.recipient,
        amount:payload.amount,
        description:payload.description,
        receive: payload.receive
    }
}

export function qrPaymentRequest(id,value,clientType)
{

    return{
        type:PaymentActionTypes.QR_PAYMENT_REQUEST,
        id,
       value,
        clientType

    }
}

const sendPaymentFail = (dispatch, {code, error}) => {

    dispatch({
        type: PaymentActionTypes.SEND_PAYMENTS_FAIL,
        payload: {code,error}
    });
};




const networkFail = (dispatch, error) => {

    dispatch({
        type: NETWORK_FAIL,
        payload: error
    });
};

