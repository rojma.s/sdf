import {PaymentActionTypes, UserActionTypes} from '../actionTypes'
import { AsyncStorage } from 'react-native';


export const emailChanged = (text) => {

    return {
        type: UserActionTypes.EMAIL_CHANGED,
        payload: text
    };
};

export const passwordChanged = (text) => {

    return {
        type: UserActionTypes.PASSWORD_CHANGED,
        payload: text
    };
};



export const userlogin = ({ umva_id, password }) => {

    return {

        type: UserActionTypes.USER_LOGIN,
        umva_id,
        password
    }



};


export const loginUserFail = () => {

    return {

        type: USER_LOGIN_FAIL,

    }
};

const networkFail = (dispatch, error) => {

    dispatch({
        type: NETWORK_FAIL,
        payload: error
    });
};



export const logout = () => {

    return{
        type:UserActionTypes.LOGOUT_REQUEST,
    }
};

export const qrRequest = (data) => {

    return {
        type: PaymentActionTypes.QR_REQUEST,
        payload: data
    };
};

export const accountBalance = (account) => {

    return {

        type: BALANCE_REQUEST,
        account
    }
};





