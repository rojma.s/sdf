import {AccountActionTypes} from "../actionTypes";

export function userAccounts(id,type) {

    switch(type)
    {
        case 'Merchant':
            return {
                type: AccountActionTypes.MERCHANT_ACCOUNTS_REQUEST,
                id
            }
        default:
            return{
                type:AccountActionTypes.CLIENT_ACCOUNTS_REQUEST,
                id
            }

    }

}
export function accountSelected(payload,type)
{

    switch (type)
    {
        case 'Merchant':
            return {
                type: AccountActionTypes.MERCHANT_ACCOUNT_CHANGE_REQUEST,
                payload
            }
        default:
            return{
                type:AccountActionTypes.CLIENT_ACCOUNT_CHANGE_REQUEST,
             payload
            }

    }
}



