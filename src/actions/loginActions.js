import {UserActionTypes} from '../actionTypes';

export function loginRequest(umvaId, password) {
  return {
    type: UserActionTypes.LOGIN.REQUEST,
    umvaId,
    password,
  }
}

export function loginSuccess({token, user}) {

  return {
    type: UserActionTypes.LOGIN.SUCCESS,
    token,
    user,
  }
}
export function loginFailure(err) {
  return {
    type: UserActionTypes.LOGIN.FAILURE,
    err,
  }
}

export function logout() {
  return {
    type: UserActionTypes.LOGOUT,
  }
}
