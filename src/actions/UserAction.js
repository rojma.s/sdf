import {UserActionTypes} from "../actionTypes";


export function userDetail(user_id)
{

    return{
        type: UserActionTypes.USER_DETAIL_REQUEST,
        user_id,
    }
};
export function userDetailSuccess(detail)
{
    return{
        type: UserActionTypes.USER_DETAILS,
        detail
    }
};
